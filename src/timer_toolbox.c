
#include "intc.h"
#include "bits.h"
#include "timer_toolbox.h"
#include "timer.h"



// Déclaration du tableau des adresses de base des timers
ulong gptimerAdresse ( unsigned short n){
	     switch (n) {


	case 1 :    return  GPT1_BASE_ADD;
	case 2 :		return  GPT2_BASE_ADD:
	case 3 :		return  GPT3_BASE_ADD;
	case 4 :		return	GPT4_BASE_ADD;
	case 5 :		return  GPT5_BASE_ADD;
	case 6 :		return	GPT6_BASE_ADD;
	case 7 :		return  GPT7_BASE_ADD;
	case 8 :		return	GPT8_BASE_ADD;
  case 9 :		return  GPT9_BASE_ADD;
	case 10 :		return  GPT10_BASE_ADD;
	case 11 :		return  GPT11_BASE_ADD;
	default : break;

						}



#define gpTimerReg(n,offsset)  (vulong*)(gptimerAdresse[(n) - 1] + (offsset)

// ------------------------------------------------------------------
//
// Fonction de démarrage du timer
//
void start_timer(uchar timer) {
	//  (TRM : 2738)
	*(gpTimerReg(timer, TCLR)) |= BIT0;
}

// ------------------------------------------------------------------
//
// Fonction d'arret du timer
//
void stop_timer(uchar timer){

	//Voir (TRM : 2738)
	*(gpTimerReg(timer, TCLR)) &= ~BIT0;
}

// ------------------------------------------------------------------
//
// lecture du timer
//
uint read_timer_value(uchar timer) {

	//  (TRM : 2740)
	return *(gpTimerReg(timer, TCRR));
}

// ------------------------------------------------------------------
//
// 'écriture  du timer
//
void write_timer_value(uchar timer, uint time){
	// (TRM : 2740)
	*(gpTimerReg(timer, TCRR)) = time;
}
