/*
 * gpio_toolbox.c
 *
 *  Created on: Apr 16, 2019
 *  Author: redsuser
 *  Modified: 27/04/2020 by  Ferchichi Ahmed Farouk
 */

#include "gpio_toolbox.h"
#include "gpio.h"
#include "intc.h"
#include "bits.h"


// cette fonction renvoie l'adresse de base
uint adresseBase (uchar module){

	switch (module){
	case 1:
	return GPIO_MOD1_BASE_ADD;
	case 2:
		return GPIO_MOD2_BASE_ADD;
	case 3:
	return GPIO_MOD3_BASE_ADD;
	case 4:
		return GPIO_MOD4_BASE_ADD;
	case 5:
		return GPIO_MOD5_BASE_ADD;
	case 6:
		return GPIO_MOD6_BASE_ADD;
	default:
		break;
	}


}

// ------------------------------------------------------------------
//
// on set les sorties d'un module  à la valeur du mask
//
void SetOutput(uchar module, ulong bitmask){

//(TRM : 3495.)
*((uint*)(OMAP_GPIO_SETDATAOUT +adresseBase(module) ))= bitmask;

}

// ------------------------------------------------------------------
//
// on met à 0 les sorties d'un module  à la valeur du mask
//
void ClearOutput(uchar module, ulong bitmask){

	// (TRM page : 3521).
	 *((uint*)(adresseBase(module) + OMAP_GPIO_CLEARDATAOUT)) = bitmask;
}

// ------------------------------------------------------------------
//
//  lire  les entrée d'un module
//
uchar ReadInput(uchar module, ulong bitmask){
	//  (TRM : 3512)
	return *((uint*)(adresseBase(module) + OMAP_GPIO_DATAIN)) & bitmask;
}

// ------------------------------------------------------------------
//
// inverser l'état d'une sortie
//
void ToggleOutput(uchar module, ulong bitmask){
	//  (TRM page  : 3513)
 *((uint*)(adresseBase(module) + OMAP_GPIO_DATAOUT))^ bitmask;
}

// ------------------------------------------------------------------
//
// Cette fonction permet d'activer des interruption
//
void UnmaskIRQ(uchar module, ulong bitmask){
	// (TRM: 3518)
	 *((uint*)(adresseBase(module) + OMAP_GPIO_SETIRQENABLE1))|= bitmask;
}

// ------------------------------------------------------------------
//
// C désactiver les interruptions
//
void MaskIRQ(uchar module, ulong bitmask){
	// (TRM: 3513)
*((uint*)(adresseBase(module) + OMAP_GPIO_CLEARIRQENABLE1)) |= bitmask;

}


// ------------------------------------------------------------------
