#include "cfg.h"
#include "stddefs.h"

#include "bits.h"
#include "init.h"
#include "lcd_toolbox.h"
#include "gpio_toolbox.h"
#include "timer_toolbox.h"

#include "gpio.h"
#include "intc.h"

void temps_reaction();
void affichage_resultat(uint time);
